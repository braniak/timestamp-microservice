function createDate(date) {
    if (isNumber(date)) {
        date = +date;
    }
    var dateObj = new Date(date);
    return dateObj;
}

function getMonth(monthNum) {
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
        'October', 'November', 'December'];
    return months[monthNum];
}

function formatDate(date) {
    return getMonth(date.getMonth()) + ' ' + date.getDate() + ' ' + date.getFullYear();
}

function isNan(num) {
    return num !== num;
}

function isNumber(str) {
    return !isNan(+str);
}

exports.isValidDate = function(date) {
    var dateObj = createDate(date);
    var timestamp = +dateObj;
    return !isNan(timestamp);
};

exports.getFullDate = function(date) {
    var dateObj = createDate(date);

    return {
        'unix': +dateObj,
        'natural': formatDate(dateObj)
    }
};