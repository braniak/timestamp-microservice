var http = require('http');
var url = require('url');
var querystring = require('querystring');
var utils = require('./utils');

var server = http.createServer(function(req, res) {
    if (req.url === '/') {
        res.setHeader('Content-type', 'text/html');
        res.end('<h1>Timestamp microservice</h1>' +
            '<h3>Example usage</h3>' +
            '<p><code>https://timestamp-microservice26.herokuapp.com/December%2016,%202015</code></p>' +
            '<p><code>https://timestamp-microservice26.herokuapp.com/1450137600</code></p>' +
            '<h3>Exampe output</h3>' +
            '<p><code>{ "unix": 1450137600, "natural": "December 15, 2015" }</code></p>');
    }
    if (req.url === '/favicon.ico') {
        res.end();
    }

    var path = url.parse(req.url.slice(1)).pathname;
    var str = querystring.unescape(path);

    if (utils.isValidDate(str)) {
        res.setHeader('Content-type', 'application/json');
        res.end(JSON.stringify(utils.getFullDate(str)));
    } else {
        res.statusCode = 500;
        res.end('Invalid request');
    }
    res.end();
});

server.listen(process.env.PORT || 3000);